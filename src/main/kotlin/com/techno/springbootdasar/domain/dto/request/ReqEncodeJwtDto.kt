package com.techno.springbootdasar.domain.dto.request

data class ReqEncodeJwtDto(
    val id: Int = 0,
    val email: String = "",
    val password: String = ""
)
