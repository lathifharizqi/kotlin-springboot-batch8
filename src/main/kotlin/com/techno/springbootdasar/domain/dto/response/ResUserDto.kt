package com.techno.springbootdasar.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty

data class ResUserDto(

    @field:JsonProperty(value = "uuid")
    val uuid : String? = null,

    @field:JsonProperty(value = "first_name")
    val firstName: String? = null,

    @field:JsonProperty(value = "last_name")
    val lastName: String? = null,

    @field:JsonProperty(value = "age")
    val age: Int? = null,

    val email: String? = null,
    val role: String? = null
)
