package com.techno.springbootdasar.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty
import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode

data class ResBaseDto<T>(
    @field:JsonProperty(value = "out_stat")
    val outStat : Boolean = StatusCode.SUCCESS.code,

    @field:JsonProperty(value = "out_code")
    val outMess : String = CommonVariable.SUCCESS_MESSAGE,

    @field:JsonProperty(value = "out_data")
    val data : T? = null
)
