package com.techno.springbootdasar.domain.dto.response

import javax.persistence.Id

data class ResDecodeJwtDto(
    val id: Int = 0,
    val email: String = ""
)
