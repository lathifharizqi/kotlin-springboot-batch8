package com.techno.springbootdasar.service

import org.springframework.stereotype.Service

interface LogicService {
    fun oddEven(number: Int): String
}