package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqLoginDto
import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResLoginDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import com.techno.springbootdasar.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("v1/api/account")
class AccountController(
    private val userService: UserService
) {
    @GetMapping
    fun getAllUser(): ResponseEntity<ResBaseDto<ArrayList<ResUserDto>>>{
        val response = userService.getUserAll()
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/getAll")
    fun getAll(): ResponseEntity<ResBaseDto<ArrayList<ResUserDto>>>{
        val response = userService.getAll()
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/{idUser}")
    fun getUserById(@PathVariable("idUser") idUser : String): ResponseEntity<ResBaseDto<ResUserDto>>{
        val response = userService.getUserById(idUser)
        return ResponseEntity.ok().body(response)
    }

    @PostMapping
    fun insertUser(@RequestBody reqUserDto: ReqUserDto): ResponseEntity<ResBaseDto<ResUserDto>> {
        val response = userService.insertUser(reqUserDto)
        return ResponseEntity.ok().body(response)
    }

    @PostMapping("/login")
    fun loginUser(@RequestBody reqLoginDto: ReqLoginDto): ResponseEntity<ResBaseDto<ResLoginDto>> {
        val response = userService.loginUser(reqLoginDto)
        return ResponseEntity.ok().body(response)
    }

    @PutMapping("/{idUser}")
    fun updateUser(
        @RequestBody reqUserDto: ReqUserDto,
        @PathVariable("idUser") idUser: String
    ): ResponseEntity<ResBaseDto<ResUserDto>> {
        val response = userService.updateUser(
            reqUserDto,
            idUser
        )
        return ResponseEntity.ok().body(response)
    }

    @DeleteMapping("/{idUser}")
    fun deleteUser(@PathVariable("idUser") idUser: String): ResponseEntity<ResBaseDto<Any>> {
        val response = userService.deleteUser(idUser)
        return ResponseEntity.ok().body(response)
    }

}